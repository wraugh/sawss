sawss is a bash function that lets you

 - load your AWS credentials from a password manager instead of storing them
   in plaintext,
 - easily switch between profiles,
 - show the active profile in your prompt, and
 - use Multi-Factor Authentication.

Documentation
-------------


The implementation of sawss is so simple that I chose to document it by
annotating the source code. The file you're reading is both the code and
its documentation. You can read it in source code form in the `sawss` file,
or markdown in the `README.md` file. To generate a pretty HTML file, run

    make docs

then open `docs/sawss.html` in your web browser.

How it works
------------


The [AWS CLI and SDKs][1] use authentication credentials from environment
variables with a fallback to the `~/.aws/credentials` and `~/.aws/config`
files. By exporting the right environment variables, we can effectively
"log in" to the AWS profile of our choice.

 [1]: https://aws.amazon.com/getting-started/tools-sdks/

These are the variables we may want to set:

```
__sawss_vars="\
    AWS_ACCESS_KEY_ID\
    AWS_SECRET_ACCESS_KEY\
    AWS_SESSION_TOKEN\
    AWS_DEFAULT_REGION\
    AWS_REGION\
    SAWSS_PROFILE\
    SAWSS_EXPIRES\
"

```

See [the doc on AWS environment variables][2] for an explanation of what they
mean. `SAWSS_PROFILE` is the name of the active profile; `SAWSS_EXPIRES`
records the session expiration date, if any.

 [2]: https://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html
 [3]: https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html

The idea is to create a function that takes an [AWS profile][3] and sets the
environment variables that log us in to that profile. To do this, it will need
to look up profile information from the aws config files:

```
__sawss_creds_file=~/.aws/credentials
if [ -n "${AWS_SHARED_CREDENTIALS_FILE}" ]; then
    __sawss_creds_file="${AWS_SHARED_CREDENTIALS_FILE}"
fi

__sawss_config_file=~/.aws/config
if [ -n "${AWS_CONFIG_FILE}" ]; then
    __sawss_config_file="${AWS_CONFIG_FILE}"
fi

```

For example, let's say we have the following `~/.aws/credentials`:

    [user1]
    aws_access_key_id = AKIAI000EXAMPLEUSER1

    [user2]
    aws_access_key_id = AKIAI000EXAMPLEUSER2

Then we want a call to

    $ sawss user1

to set the `AWS_ACCESS_KEY_ID` environment variable to
`AKIAI000EXAMPLEUSER1`.


Securing secret access keys
---------------------------


The AWS doc is full of examples showing secret access keys [stored in
plaintext in the credentials file][4]. This is a terrible
practice, and we can do better: we can dynamically obtain the secrets as
needed from [pass, our password store][5].

 [4]: https://docs.aws.amazon.com/cli/latest/userguide/cli-config-files.html
 [5]: https://www.passwordstore.org/

We can create passwords whose name is
the access key id, and whose value is the secret access key. Then we can
lookup the secrets like this:

```
__sawss_reqs="pass"   # the password store
__sawss_reqs+=" iniq" # to read from the aws config https://gitlab.com/wraugh/iniq

__sawss_get_key_id() {
    profile="$1"

    iniq "$profile.aws_access_key_id" < "$__sawss_creds_file"
}

__sawss_get_key_secret() {
    key_id="$1"

    pass show "$(__sawss_get_pass_prefix)${key_id}" | head -n1
}

__sawss_get_pass_prefix() {
    if [ -n "${SAWSS_PASS_PREFIX}" ]; then
        echo "${SAWSS_PASS_PREFIX}/"
    fi
}

```

Continuing the example from above, we can store the secret access key for
user1 in `pass`:

    $ pass insert keys/aws/AKIAI000EXAMPLEUSER1
    Enter password for keys/aws/AKIAI000EXAMPLEUSER1:
    Retype password for keys/aws/AKIAI000EXAMPLEUSER1:

    $ export SAWSS_PASS_PREFIX=keys/aws
    $ sawss user1
    $ env | grep AWS
    SAWSS_PROFILE=user1
    AWS_ACCESS_KEY_ID=AKIAI000EXAMPLEUSER1
    AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

This allows us to keep our secret access keys encrypted at rest.


Multi-Factor Authentication
---------------------------


AWS lets you [require MFA for your account][6]. You give it
your secret access key plus a one-time password, and it returns a temporary
session token (STS). Then you use that session token to sign your requests.

 [6]: https://docs.aws.amazon.com/IAM/latest/UserGuide/tutorial_users-self-manage-mfa-and-creds.html

If an attacker gets a hold of your session token, they can only use it for a
few hours before it expires. If they get a hold of your secret access key,
they can't do anything with it unless they also have your OTP generator. This
can help you secure your account.

Let's create a function that takes an AWS profile and a one-time password,
and starts an STS session:

```
__sawss_reqs+=" aws" # to start STS sessions https://github.com/aws/aws-cli
__sawss_reqs+=" jq"  # to parse session data https://stedolan.github.io/jq/

__sawss_start_sts_session() {
    profile="$1"
    otp="$2"

    key=$(__sawss_get_key_id "$profile")
    duration=
    if [ -n "$SAWSS_SESSION_DURATION" ]; then
        duration="--duration-seconds=$SAWSS_SESSION_DURATION"
    fi
    workfile=$(mktemp)

    AWS_ACCESS_KEY_ID="$key" \
        AWS_SECRET_ACCESS_KEY=$(__sawss_get_key_secret "$key") \
        aws sts get-session-token > "$workfile" \
        "$duration" \
        --serial-number="$(__sawss_get_mfa_device_id "$profile")" \
        --token-code="$otp" \
        --output=json

    export AWS_ACCESS_KEY_ID=$(jq -r .Credentials.AccessKeyId < "$workfile")
    export AWS_SECRET_ACCESS_KEY=$(jq -r .Credentials.SecretAccessKey < "$workfile")
    export AWS_SESSION_TOKEN=$(jq -r .Credentials.SessionToken < "$workfile")
    export SAWSS_EXPIRES=$(jq -r .Credentials.Expiration < "$workfile" | sed 's/[^0-9]*//g')

    rm "$workfile"
}

```

We can tie an MFA device to an AWS profile by adding a custom property to the
profile in the credentials file. Let's call it `mfa_device_id`. Here's what
`~/.aws/credentials` would look like in our example:

    [user1]
    aws_access_key_id = AKIAI000EXAMPLEUSER1
    mfa_device_id = arn:aws:iam::999999999999:mfa/user1

And the function to read that property:

```
__sawss_get_mfa_device_id() {
    profile="$1"

    iniq "$profile.mfa_device_id" < "$__sawss_creds_file"
}

```

Tying it all together
---------------------


```
sawss() {
    profile="$1"
    otp="$2"

    __sawss_clear_session
    if [ -z "$profile" ]; then
        return
    fi

    export SAWSS_PROFILE="$profile"
    __sawss_set_region "$profile"

    if [ -n "$otp" ]; then
        __sawss_start_sts_session "$profile" "$otp"
    else
        export AWS_ACCESS_KEY_ID=$(__sawss_get_key_id "$profile")
        export AWS_SECRET_ACCESS_KEY=$(__sawss_get_key_secret "$AWS_ACCESS_KEY_ID")
    fi
}

__sawss_set_region() {
    profile="$1"

    region=$(iniq "$profile.region" < "${__sawss_config_file}")
    if [ -n "$region" ]; then
        export AWS_DEFAULT_REGION="$region"
        export AWS_REGION="$region"
    fi
}

__sawss_clear_session() {
    for var in $__sawss_vars; do
        unset "$var"
    done
}


```

Bonus: here's a helper to show the current AWS profile in our prompt:

```
__sawss_ps1() {
    if [[ -n "$SAWSS_PROFILE" ]]; then
        ret=" $SAWSS_PROFILE"

        now=$(date -u +%Y%m%d%H%M%S)
        if [ -n "$SAWSS_EXPIRES" ]; then
            if [ $now -gt $SAWSS_EXPIRES ]; then
                ret+=" (exp)"
            fi
        fi

        echo "$ret"
    fi
}

```

Usage & configuration
---------------------


To use sawss, source the `sawss` file. This will define the sawss function.
You may want to set values for these variables:

 - `SAWSS_SESSION_DURATION`: how long (in seconds) STS sessions should last
 - `SAWSS_PASS_PREFIX`: the prefix of your secret key's name in `pass`

Requirements
------------


Make sure you have the requirements installed:
 
 - [aws-cli](https://github.com/aws/aws-cli)
 - [jq](https://stedolan.github.io/jq/)
 - [pass](https://passwordstore.org)
 - [iniq](https://gitlab.com/wraugh/iniq)

In fact, let's give ourselves a warning if any of these aren't installed:

```
for req in $__sawss_reqs; do
    if ! type $req >/dev/null 2>/dev/null; then
        2>&1 echo "Can't find $req installed; sawss will not work."
    fi
done

```

License
-------


This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.


