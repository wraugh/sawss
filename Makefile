README.md: node_modules sawss
	./node_modules/.bin/docco -l plain-markdown -o . -e .sh sawss > /dev/null; \
		mv sawss.html README.md

docs: node_modules sawss
	./node_modules/.bin/docco -l linear -e .sh sawss

node_modules:
	npm install
